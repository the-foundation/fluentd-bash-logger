#!/bin/bash
## logger that sends STDIN to influxdb2 http(s):// endpoints with optional TOKEN auth
[[ -z "INFLUX_USER_AGENT" ]] && INFLUX_USER_AGENT="influx_"$(echo $( curl --version|head -n1|cut -d" " -f1,2;curl --version |head -n2 |tail -n1|grep ^Release|cut -d":" -f2)|sed "s~ ~_~g")
## STAGE:PREPARE
URL=$1
BUCKET=$2
ORG=$3
INSECURE=$4
TAG=$5
AUTH=$6
HOST=$7
SEVERITY=$8

[[ -z "$INFLUX_MEASUREMENT" ]] && INFLUX_MEASUREMENT="log"

[[ -z "$URL" ]]                && echo "FAILED:NO_URL_GIVEN"
[[ -z "$URL" ]]                && exit 1

[[ -z "$BUCKET" ]]             && echo "FAILED:NO_BUCKET_GIVEN"
[[ -z "$BUCKET" ]]             && exit 1

[[ -z "$ORG" ]]                && echo "FAILED:NO_ORG_GIVEN"
[[ -z "$ORG" ]]                && exit 1

[[ -z "$TAG" ]]                && echo "FAILED:NO_TAG_GIVEN"
[[ -z "$TAG" ]]                && exit 1

[[ -z "$AUTH" ]]               || TOK="$AUTH"
[[ -z "$SEVERITY" ]]           || SEVPART=",severity=$SEVERITY"
[[ -z "$HOST" ]]               || MYHOST="$HOST"
[[ -z "$HOST" ]]               && MYHOST="$(hostname -f)"


[[ "$ORG" = "FALSE" ]]                && echo "FAILED:org_name_FALSE_is_forbidden"
[[ "$ORG" = "FALSE" ]]                && exit 1

[[ "$INSECURE" = "INSECURE" ]] &&  SSLPART=" -k "

while IFS='' read line;do 
    msg=$(echo -n "$line"|jq -R -s '.' )
    echo "${INFLUX_MEASUREMENT}"',tag='"$TAG"',host='"$MYHOST$SEVPART"' value='"$msg"' '$(date +%s%N) | curl -s -A "${INFLUX_USER_AGENT}" --retry-delay 30 --retry 3 -X POST   $SSLPART --header "Authorization: Token $TOK"  "$URL"'/api/v2/write?org='"$ORG"'&bucket='"$BUCKET"'&precision=ns' -s  --data-binary @/dev/stdin 
done
