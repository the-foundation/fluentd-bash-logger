 #!/bin/bash
 
URL=$1

[[ -z "$LOKI_AUTH"         ]]  && AUTH=$2
[[ -z "$LOKI_AUTH"         ]]  || AUTH=$LOKI_AUTH
[[ -z "$LOKI_SERVICE"      ]]  && LOKI_SERVICE=shell_output

[[ -z "$LOKI_HOST"       ]]  && HOST="$(hostname -f)"
[[ -z "LOKI_USER_AGENT" ]]  && LOKI_USER_AGENT="influx_"$(echo $( curl --version|head -n1|cut -d" " -f1,2;curl --version |head -n2 |tail -n1|grep ^Release|cut -d":" -f2)|sed "s~ ~_~g")

 #curl -v -H "Content-Type: application/json" -XPOST -s "http://localhost:3100/loki/api/v1/push" --data-raw \
 # '{"streams": [{ "stream": { "foo": "bar2" }, "values": [ [ "1570818238000000000", "fizzbuzz" ] ] }]}'
 

while IFS='' read line;do 
    lpart=' ' 
    rpart='  '
    jsonline=$(echo -n "$line" |jq -R -s -c  '{"log": . }')
    jq -n --arg msg "$jsonline"    '{"streams": [{ "stream": { "log": "main" }, "values": [ [ "'"$(date +%s)""000000000"'", $msg ,  { "host": "'"$HOST"'" , "service": "'"$LOKI_SERVICE"'"  } ] ] }]}'|  curl \
      -s -A "${LOKI_USER_AGENT}" --retry-delay 30 --retry 3 -X POST   $SSLPART -u $AUTH  -H "Content-Type: application/json" -X POST -s "$URL" --data-binary @/dev/stdin
done
