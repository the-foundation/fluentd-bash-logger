#!/bin/bash
## logger that sends STDIN to fluentd http(s):// endpoints with optional HTTP Basic AUTH

## STAGE:PREPARE
URL=$1
TAG=$2
INSECURE=$3
AUTH=$4
MYHOST=$5


[[ -z "$URL" ]]               && echo "FAILED:NO_URL_GIVEN"
[[ -z "$URL" ]]               && echo "EXIT 1"

[[ -z "$TAG" ]]               && echo "FAILED:NO_URL_GIVEN"
[[ -z "$TAG" ]]               && echo "EXIT 1"

[[ -z "$AUTH" ]]              || AUTHPART=" -u $AUTH "
[[ -z "$MYHOST" ]]            || myhost="$MYHOST"

[[ "$INSECURE" = "INSECURE" ]] &&  SSLPART=" -k "

MYURL="$URL/$TAG"

echo "$MYURL"|grep -e ^http: -e ^https: -q || echo "FAILED:URL_NOT_HTTP(s)"
echo "$MYURL"|grep -e ^http: -e ^https: -q || exit 1

[[ -z "$myhost" ]] && myhost=$(hostname -f) 

## STAGE:MAIN
while IFS='' read INCOMINGLINE;do 
     sdate=$(date +%s)
     ndate=$(date +%N)
     [[ -z "$ndate"  ]] && ndate=000000000
     SENDURL=$MYURL"?time="$sdate"."$ndate
     curl -s --retry-delay 30 --retry 3 -X POST -H 'Content-Type: multipart/form-data'  -F 'json=[{"host":"'"$myhost"'","message":"'"$(echo -n ${INCOMINGLINE}|jq -R -s '.' -c --raw-output  )"'"}]'  "$SENDURL"  $AUTHPART  ;
done