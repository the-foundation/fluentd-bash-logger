# FluentD+Influxdb-bash-logger

### INFLUXdb2

#### Parameters 

* URL=$1
* BUCKET=$2
* INSECURE=$3
* TAG=$4
* AUTH=$5
* HOST=$6
* SEVERITY=$7


#### Example
```
bash log-to-influxdb2.sh https://my-logger.td-logthingy.tld logbucket TRUE logtag ThatLongInfluxTokenYouGotCopiedFromTheSite differenthostname medium
```

### FluentD / td-agent / fluent-bit


#### Parameters 

* URL=$1
* TAG=$2
* INSECURE=$3
* AUTH=$4

#### Example
```
bash log-to-fluent.sh https://my-logger.td-logthingy.tld syslog TRUE username:somerandompass
```
## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/the-foundation/fluentd-bash-logger/-/settings/integrations)
